## Self-Signed Server Certificates

To create a server providing SSL-encrypted connection services, you will need to acquire a certificate for that service. There are many ways of acquiring appropriate certificates, such as buying one from a certification authority. Another common practice is to generate a self-signed certificate. The simplest way to do this is with the OpenSSL package:

```bash
# Generate OpenSSL private key
openssl genrsa -des3 -out server.orig.key 2048
openssl rsa -in server.orig.key -out server.key

# Generate certificate signing request (CSR)
# when prompted for the 'Common Name' provide the server's FQDN
openssl req -new -key server.key -out server.csr

# Self-sign your certificate
openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.pem

# Review
openssl x509 -text -noout -in server.pem
```

Now put the server's certificate and private key in a `certs/` directory under the root of this project:

```bash
mkdir certs
mv server* ./certs 
```

Finally, append the server's certificate to a shared CA cert file accessible from all clients:

```bash
cat server.pem >> path/to/shared/cacert.pem
```

**N.B.** Running OpenSSL on Windows through GitBash may lead to the console hanging. If that is the case, prefix all OpenSSL commands with ``winpty``
