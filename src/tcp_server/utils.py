import ipaddress
import json
import pathlib
import socket

from functools import wraps
from typing import Callable


def host_in_acl(host: str, acl_path: str = 'acl.txt') -> bool:
    """Check if given IPv4 host address is allowed by the Access Control List (ACL).

    Parameters
    ----------
    host : str
        IPv4 address
    acl_path : str, optional
        path to the access control list file, by default 'acl.txt'

    Returns
    -------
    bool
        wether the given host is allowed by the ACL
    """

    with open(acl_path) as f:

        for line in f:
            if not line.startswith("#"):
                if ipaddress.ip_address(host) in ipaddress.ip_network(line.rstrip()):
                    return True 

    return False


def pad(buffer: bytes, length: int) -> bytes:
    """Pad input buffer to set length.

    Parameters
    ----------
    buffer : bytes
        byte string to pad
    length : int
        length of output byte string

    Returns
    -------
    bytes
        padded byte string of given length
    """

    input_len = len(buffer)
    padding_len = length - input_len

    if padding_len < 0:
        raise ValueError("Buffer exceeds maximum size.")

    return buffer + b' ' * padding_len


def tag(sock: socket.socket, peer=True) -> str:
    """Return host-side or peer-side socket connection information string.

    Parameters
    ----------
    sock : socket.socket
        socket connection
    peer : bool, optional
        True to get peer-side information rather than host-side, by default True.

    Returns
    -------
    str
        host or peer information
    """

    sock_getname = sock.getpeername if peer else sock.getsockname
    
    host, port = sock_getname()
    hostname = socket.getfqdn(host)

    return f"{hostname}@{host}:{port}"


def keyboard_interrupt(f: Callable) -> Callable:
    """Wrap function to except KeyboardInterrupts.

    Parameters
    ----------
    f : Callable
        function to wrap

    Returns
    -------
    Callable
        wrapping function
    """

    @wraps(f)
    def wrapper(*args, **kwargs):
        
        try:
            f(*args, **kwargs)
        
        except KeyboardInterrupt:
            print("\n Received KeyboardInterrupt")
    
    return wrapper


def load_json(fn: str):
    """Load data from json file

    Parameters
    ----------
    fn : str
        path to file
    """

    # Read data from file
    if pathlib.Path(fn).is_file():
        
        if fn.endswith('.json'):
        
            with open(fn) as d:
                return json.load(d)
                
        else:
            raise FileNotFoundError(f"Unsupported file type `{fn}`")

    else:
        raise ValueError(f"`{fn}` not found")