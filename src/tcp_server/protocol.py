import json
import socket
from typing import Any, Dict, NamedTuple

from . import server
from . import utils

# Socket protocol config
TCP, IPv4 = socket.SOCK_STREAM, socket.AF_INET
PROTOCOL, ADDRESS_FAMILY = TCP, IPv4

# Message config
CONNECTION_ACCEPTED_MESSAGE = "!ACCEPTED"
CONNECTION_REFUSED_MESSAGE = "!REFUSED"
PROCESSING_RECIPE = "!PROCESSING RECIPE"
INVALID_RECIPE = "!INVALID RECIPE"
EOT_MESSAGE = "!EOT"  # end of task aknolwedgment
DISCONNECT_MESSAGE = "!DISCONNECT"  # disconnection token message

# Packet config
# size of the packet protoheader, in bytes
SIZE_PROTOHEADER = 4  # i.e. max SIZE_HEADER = 9999 bytes
FORMAT = 'utf-8'  # encoding format for data sent across the socket

# Schema for packet header fields
class HeaderFields(NamedTuple):

    SIZE_PAYLOAD: int
    PAYLOAD_SER_TYPE: str
    PAYLOAD_DESER_TYPE: str


# Schema for packet structure
class Packet(NamedTuple):

    protoheader: bytes
    header: bytes
    payload: bytes


def receive(sock: socket.socket) -> Any:
    """Receive object across socket.

    Parameters
    ----------
    sock : socket.socket
        socket connection

    Returns
    -------
    Any
        objet received
    """

    packet = receive_packet(sock)

    data = parse_payload(packet.payload)
    type = parse_header(packet.header).PAYLOAD_DESER_TYPE

    obj = deserialise(data, type)

    print(f"[RECEIVED] {obj}")

    return obj


def deserialise(data: str, type: str) -> Any:
    """Deserialise data.

    Parameters
    ----------
    data : str
        serialised data
    type : str
        deserialisation method

    Returns
    -------
    Any
        output object

    Raises
    ------
    ValueError
        if `type` value not supported
    """

    try:

        if type == 'raw':

            return data
        
        elif type == 'json':

            return json.loads(data)
        
        elif type == 'recipe':

            d = json.loads(data)
            return server.Recipe(**d)
            
        else:

            raise ValueError(f"Unsupported data deserialisation method `{type}`")
    
    except TypeError:
        print(f"[DESERIALISATION ERROR] Data could not be deserialised")
        return None



def parse_protoheader(protoheader: bytes) -> int:
    """Parse packet protoheader.

    The packet protoheader is a SIZE_PROTOHEADER-bytes utf-8 
    encoded integer representing the size (in bytes) of the 
    packet header.

    Parameters
    ----------
    protoheader : bytes
        packet protoheader

    Returns
    -------
    int
        packet header size, in bytes
    """

    size_header =  int(protoheader.decode(FORMAT))

    return size_header


def parse_header(header: bytes) -> HeaderFields:
    """Parse packet header.

    The packet header is a utf-8 encoded serialised json
    whose schema is defined in HeaderFields.

    Parameters
    ----------
    header : bytes
        packet header

    Returns
    -------
    HeaderFields
        named tuple of protocol header fields and values
    """

    fields = json.loads(header.decode(FORMAT))

    return HeaderFields(**fields)


def parse_payload(payload: bytes) -> str:
    """Parse packet payload

    Parameters
    ----------
    payload : bytes
        packet payload

    Returns
    -------
    str
        serialised data
    """

    data = payload.decode(FORMAT)

    return data



def receive_packet(sock: socket.socket) -> Packet:
    """Receive packet across socket.

    Parameters
    ----------
    sock : socket.socket
        socket connection

    Returns
    -------
    Packet
        packet
    """

    # Read in packet protoheader
    protoheader = sock.recv(SIZE_PROTOHEADER)  # Blocking until received data

    if protoheader:

        # Parse protoheader for information about header
        size_header = parse_protoheader(protoheader)
        header = sock.recv(size_header)

        # Parse header for information about payload
        size_payload = parse_header(header).SIZE_PAYLOAD 
        payload = sock.recv(size_payload)

    else:
        header = b''
        payload = b''

    return Packet(protoheader, header, payload)


def send_packet(sock: socket.socket, packet: Packet) -> None:
    """Send packet across socket.

    Parameters
    ----------
    sock : socket.socket
        socket connection
    packet: Packet
        packet to send
    """
    
    # Send packet over socket
    sock.send(packet.protoheader)
    sock.send(packet.header)
    sock.send(packet.payload)


def build_protoheader(header: bytes) -> bytes:
    """Build packet protoheader associated to given header.

    The packet protoheader is a SIZE_PROTOHEADER-bytes utf-8 
    encoded integer representing the size (in bytes) of the 
    packet header.

    Parameters
    ----------
    header : bytes
        packet header

    Returns
    -------
    bytes
        packet protoheader
    """
    
    size_header = len(header)

    protoheader = str(size_header).encode(FORMAT)

    return utils.pad(protoheader, SIZE_PROTOHEADER)


def build_header(payload: bytes, ser_type: str, deser_tpye: str) -> bytes:
    """Build packet header associated to given payload.

    Packet header is a utf-8 encoded serialised json
    whose schema is defined in HeaderFields.

    Parameters
    ----------
    payload : bytes
        packet payload
    ser_type : str
        data serialisation method
    deser_tpye: str
        data deserialisation method

    Returns
    -------
    bytes
        packet header
    """

    size_payload = len(payload)
    payload_ser_type = ser_type
    payload_deser_type = deser_tpye

    fields = HeaderFields(
        SIZE_PAYLOAD=size_payload, 
        PAYLOAD_SER_TYPE=payload_ser_type,
        PAYLOAD_DESER_TYPE=payload_deser_type,
    )

    header  = json.dumps(fields._asdict())

    return header.encode(FORMAT)


def build_packet(data: str, ser_type: str, deser_tpye: str) -> Packet:
    """Build packet from serialised data.

    Parameters
    ----------
    data : str
        serialised data
    ser_type : str
        data serialisation method
    deser_tpye: str
        data deserialisation method

    Returns
    -------
    Packet
        packet
    """

    payload = data.encode(FORMAT)


    header = build_header(payload, ser_type, deser_tpye)

    protoheader = build_protoheader(header)

    return Packet(protoheader, header, payload)


def serialise(obj: Any, type: str) -> str:
    """Serialise data.

    Parameters
    ----------
    obj : Any
        input object
    type : str
        serialisation method

    Returns
    -------
    str
        serialised data

    Raises
    ------
    ValueError
        if `type` value not supported
    """

    if type == 'raw':

        return obj
    
    elif type == 'json':

        return json.dumps(obj)
    
    else:

        raise ValueError(f"Unsupported data serialisation method `{type}`")


def send(sock: socket.socket, obj: Any, ser_type: str, deser_type: str = None) -> None:
    """Send object across socket.

    Parameters
    ----------
    sock : socket.socket
        socket connection
    obj : Any
        object to send
    ser_type : str
        object serialisation method
    deser_tpye: str
        object deserialisation method
    """

    # Mirror serialisation by default
    deser_type = deser_type if deser_type else ser_type

    data = serialise(obj, ser_type)

    packet = build_packet(data, ser_type, deser_type)

    send_packet(sock, packet)

    print(f"[SENT] {obj}")