"""
CLI demo:

Spin-up server with:
    poetry run tcp server start

Send client request with:
    poetry run tcp client execute "Hello World!"
"""

import click

from . import client as clientlib
from . import utils
from . import server as serverlib


BANNER = r"""
 ______________    _________ 
/_  __/ ___/ _ \ _/_/  _/ _ \
 / / / /__/ ___//_/_/ // ___/
/_/  \___/_/  /_/ /___/_/    
                             
"""

SERVER = 'localhost'  # default server fqdn
PORT = 65432  # Port to listen on (non-privileged ports are > 1023)
CAFILE = 'certs/cacert.pem'  # path to CA cert file


@click.group()
def main() -> None:
    """Launch the TCP/IP server CLI"""

    click.echo(BANNER)

    pass


@main.group()
def server() -> None:
    """TCP/IP server CLI"""

    pass


@server.command()
@click.option("-p", "--port", default=PORT, help="port on which to run the server")
def start(port) -> None:
    """Start TCP/IP server"""

    serverlib.start_server(port=port)

    click.echo()


@server.command()
def info() -> None:
    """Get TCP/IP server info"""

    click.echo("No TCP/IP server info available")


@main.group()
def client() -> None:
    """TCP/IP client CLI"""

    pass


@client.command()
@click.argument("payload")
@click.option("-f", "--file", default=False, is_flag=True, help="payload has been provided as file")
@click.option("-h", "--hostname", default=SERVER, help="FQND of the host to which to connect")
@click.option("-p", "--port", default=PORT, help="port on which server is running")
@click.option("-ca", "--cafile", default=CAFILE, help="path to CA certificates file")
def send(payload, file, hostname, port, cafile) -> None:
    """Send recipe command to TCP/IP server"""

    if file:
        payload = utils.load_json(payload)
        ser_type = 'json'
    else:
        ser_type = 'raw'
    
    clientlib.e2e(obj=payload, ser_type=ser_type, deser_type='recipe', hostname=hostname, port=port, cafile=cafile)

    click.echo()