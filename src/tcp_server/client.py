
from contextlib import contextmanager
import pathlib
import socket
import ssl
from typing import Any


from . import protocol
from . import utils


def create_context(cafile: str) -> ssl.SSLContext:
    """Return a new SSL Context object used to authenticate Web servers.

    The CA certificates file should include the certificate of the FQDN
    of the Web server for which the SSL context is being created. 

    Parameters
    ----------
    cafile : str
        path to a file of concatenated CA certificates in PEM format.
    
    Returns
    -------
    ssl.SSLContext
        SSL Context object used to create client-side sockets
    """

    # Check CA cert file exists
    if not pathlib.Path(cafile).is_file():
        raise FileNotFoundError(f"Could not find CA cert file: `{cafile}`")

    ctx = ssl.create_default_context(purpose=ssl.Purpose.SERVER_AUTH, cafile=cafile)
    
    return ctx


@ contextmanager
def create_client(hostname: str, cafile: str):
    """Yield a TCP IPv4 socket ready to initiate TLS connections with the given server.

    Parameters
    ----------
    hostname : str
        fully qualified domain name (FQDN) of the host to which to connect
    cafile : str
        path to a file of concatenated CA certificates in PEM format.

    Yields
    -------
    ssl.SSLSocket
        client-side ssl socket
    """

    try:

        # Spin-up socket connection
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0) as sock:
        
            # Create ssl protocol context
            ctx = create_context(cafile=cafile)

            # Wrap TLS encryption
            with ctx.wrap_socket(sock, server_hostname=hostname) as ssock:

                yield ssock
    
    finally:
        pass


@contextmanager
def connection_manager(sock: socket.socket, hostname: str, port: int):
    """Manage connection to host.

    Host should respond with a CONNECTION_ACCEPTED_MESSAGE or 
    a CONNECTION_REFUSED_MESSAGE.

    Parameters
    ----------
    sock : socket.socket
        socket to use for connection
    hostname : str
        fully qualified domain name (FQDN) of the host to which to connect
    port : int
        port on which host application is running

    Yields
    -------
    None
        on connection

    Raises
    ------
    ConnectionRefusedError
        if connection is refused by host
    ValueError
        if protocol error
    """
    
    # Get IPv4 address of host
    host = socket.gethostbyname(hostname)
    tag = f"{hostname}@{host}:{port}"

    try:

        print(f"[CONNECTING] Connecting to {tag}")
        
        # Connect to host
        sock.connect((hostname, port))
        token = protocol.receive(sock)
        
        if token == protocol.CONNECTION_ACCEPTED_MESSAGE:
            print(f"[CONNECTION ACCEPTED] Connected to {tag}\n")

            yield
    
        elif token == protocol.CONNECTION_REFUSED_MESSAGE:
            print(f"[CONNECTION REFUSED] Connection not allowed to {tag}\n")
            raise ConnectionRefusedError("Connection refused by host")

        else:
            raise ValueError(f"Received unexpected token `{token}`")

    finally:
        # Send disconnection token to close socket
        print(f"[DISCONNECTING] Initiating disconnection from {tag}")
        send(sock, protocol.DISCONNECT_MESSAGE, ser_type='raw')



@contextmanager
def start_client(hostname: str, port: int, cafile: str):
    """Start a TCP/IP client connected to given host and port.

    Client is ready to send objects on socket.

    Parameters
    ----------
    hostname : str
        fully qualified domain name (FQDN) of the host to which to connect
    port : int
        port on which host application is running
    cafile : str
        path to a file of concatenated CA certificates in PEM format.

    Yields
    -------
    ssl.SSLSocket
        socket connection to host
    """

    try:

        # Spin-up client socket
        with create_client(hostname, cafile) as ssock:

             # Open connection to host (automatically closed)
            with connection_manager(ssock, hostname, port):

                yield ssock
    
    finally:
        pass


@utils.keyboard_interrupt
def send(sock: socket.socket, obj: Any, ser_type: str, deser_type: str = None):
    """Send payload over socket connection, waiting for server to process it.
 
    Parameters
    ----------
    sock : socket.socket
        [description]
    obj : Any
        [description]
    ser_type : str
        [description]
    deser_type : str, optional
        [description], by default None
    """

    with transaction_manager(sock):

        protocol.send(sock, obj, ser_type, deser_type)


@contextmanager
def transaction_manager(sock: socket.socket):

    try:

        yield
    
    finally:

        # Block in listening mode until server processes task
        while True:
            
            data = protocol.receive(sock)

            if data == protocol.EOT_MESSAGE:
                print("")
                break


def e2e(obj: Any, ser_type: str, deser_type: str, hostname: str, port: int, cafile: str) -> None:
    """Execute end-to-end transaction with given host.

    An ephemeral client is spun up to execute the transaction and is torn down on completion.

    Parameters
    ----------
    obj : Any
        object to send over the socket connection
    ser_type: str
        serialisation method
    deser_type: str
        deserialisation method
    hostname : str
        fully qualified domain name (FQDN) of the host to which to connect
    port : int
        port on which host application is running
    cafile : str
        path to a file of concatenated CA certificates in PEM format.
    """

    print("[STARTING] Client is starting...\n")

    # Spin-up client socket and connect
    with start_client(hostname, port, cafile) as ssock:
  
        # blocks until received EOT response from server
        send(ssock, obj, ser_type, deser_type)
    
    print(f"[DISCONNECTED] Client is disconnected")
