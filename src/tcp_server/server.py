from contextlib import contextmanager
import pathlib
import socket
import ssl
import threading
from typing import Any, Dict, NamedTuple

from . import protocol
from . import utils


# Schema for recipes accepted by server
class Recipe(NamedTuple):

    TARGET: str
    CMD: str
    ARGS: Dict[str, Any]



def create_context() -> ssl.SSLContext:
    """Return a new SSL Context object used to authenticate Web clients.

    The server's certificate and private key are expected to be in a ``certs/`` directory.

    Returns
    -------
    ssl.SSLContext
        SSL Context object used to create server-side sockets
    """

    # Check server certificate exists
    CERTFILE = 'certs/server.pem'
    if not pathlib.Path(CERTFILE).is_file():
        raise FileNotFoundError(f"Could not find server certificate: `{CERTFILE}`")
    
    # Check server private key exists
    KEYFILE = 'certs/server.key'
    if not pathlib.Path(KEYFILE).is_file():
        raise FileNotFoundError(f"Could not find server private key: `{KEYFILE}`")

    ctx = ssl.create_default_context(purpose=ssl.Purpose.CLIENT_AUTH)
    ctx.load_cert_chain(certfile='certs/server.pem', keyfile='certs/server.key')
    
    return ctx


@contextmanager
def create_sserver(port: int):
    """Yield a TCP IPv4 socket bound to the given port.
    
    The socket is ready to accepet server-side SSL connections with clients.

    Parameters
    ----------
    port : int
        port on which to bind the socket

    Yields
    -------
    ssl.SSLSocket
        server-side ssl socket
    """

    try:
    
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0) as sock:
            
            # Bind service to given port on all available host interfaces (localhost, NIC, etc...)
            sock.bind(('', port))

            # Place socket in listening mode
            sock.listen()
            print(f"[LISTENING] Server is running on {socket.getfqdn()}:{port}\n")

            # Create ssl protocol context
            ctx = create_context()

            # Wrap TLS encryption
            with ctx.wrap_socket(sock, server_side=True) as ssock:

                yield ssock
    
    finally:
        pass


@utils.keyboard_interrupt
def start_server(port: int) -> None:
    """Start a TCP/IP server running on given host and port.

    Parameters
    ----------
    port : int
        port on which to run the server
    """

    print("[STARTING] Server is starting...")
    
    with create_sserver(port) as ssock:
        
        # Accept clients
        while True:
            
            c_sock, _ = ssock.accept()  # Blocking until client connection

            # Spawn a thread for each client connecting
            thread = threading.Thread(target=client_thread, args=(c_sock, ))
            thread.start()

            print(f"[ACTIVE CONNECTIONS] {threading.activeCount() - 1}")


@contextmanager
def allow(sock: socket.socket):
    """Validate peer connection against Access Control List.

    Parameters
    ----------
    sock : socket.socket
        socket to client

    Yields
    -------
    socket.socket
        socket to client if allowed by ACL

    Raises
    ------
    ConnectionRefusedError
        if refused by ACL
    """

    try:

        with sock:

            # Get client-side information    
            host, _ = sock.getpeername()
            
            tag = utils.tag(sock, peer=True)
            print(f"[HIT] Connection attempt from {tag}.")

            # Check ACL

            if utils.host_in_acl(host):
                print(f"[CONNECTION ACCEPETED] ACL accepted connection from {tag}")
                protocol.send(sock, protocol.CONNECTION_ACCEPTED_MESSAGE, ser_type='raw')
                print('')

                yield sock
            
            else:
                print(f"[CONNECTION DENIED] ACL rejected connection from {tag}")
                protocol.send(sock, protocol.CONNECTION_REFUSED_MESSAGE, ser_type='raw')
                
                raise ConnectionRefusedError

    finally:
        pass


def client_thread(sock: socket.socket) -> None:
    """Handle client upon connection.

    This function also handles protocol messages expected during the client/server 
    interaction.

    Parameters
    ----------
    sock : socket.socket
        socket to client
    """

    # Client must be validated by ACL
    with allow(sock) as sock:

        # Read-in data from client until client signals to terminate connection
        while True:

            # Make sure client is notified when server finishes processing this data
            with transaction_manager(sock):

                obj = protocol.receive(sock)

                if obj == protocol.DISCONNECT_MESSAGE:
                    break
                
                elif type(obj) == Recipe:
                    print("[VALID RECIPE] Processing command...")
                    protocol.send(sock, protocol.PROCESSING_RECIPE, ser_type='raw')

                    process_recipe(sock, obj)
                
                else:
                    print("[INVALID COMMAND] Command ignored")
                    protocol.send(sock, protocol.INVALID_RECIPE, ser_type='raw')

        print(f"[CONNECTION CLOSED] Closed connection to {utils.tag(sock, peer=True)}.\n")


@contextmanager
def transaction_manager(sock: socket.socket):

    try:

        yield

    finally:

        # Send end of task token to notify server ready for next object
        protocol.send(sock, protocol.EOT_MESSAGE, ser_type='raw')
        print("")


def process_recipe(sock: socket.socket, recipe: Recipe) -> None:
    """Handle object received over the socket.

    This function is tasked with interpetring the incoming object 
    and take appropriate action depending on the nature of
    the request.

    Parameters
    ----------
    sock : socket.socket
        socket
    obj : Any
        object received over the socket
    """

    print(f"[PROCESSING RECIPE] {recipe.CMD}{recipe.ARGS} -> {recipe.TARGET}")

    #TODO: dispatch CMDs to actions


    